@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        @include('component.form.inputtext', ['label' => 'Username'], ['name' => 'username'])
                        @include('component.form.inputemail', ['label' => 'Email'], ['name' => 'email'])
                        @include('component.form.inputpassword', ['label' => 'Password'], ['name' => 'password'])
                        @include('component.form.inputpassword', ['label' => 'Confirm Password'], ['name' => 'password_confirmation'])
                        
                        @include('component.form.submitbtn', ['text' => 'Register'])
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
