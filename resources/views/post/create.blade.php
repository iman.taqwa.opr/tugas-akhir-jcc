@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> Create Post</div>

                <div class="card-body">
                    <form method="POST" action="/post" enctype="multipart/form-data">
                        @csrf

                        
                        @include('component.form.inputfile', ['name' => 'image'], ['label' => 'Image'])

                        @include('component.form.textarea', ['name' => 'caption'], ['label' => 'Caption kamu'])

                        @include('component.form.submitbtn', ['text' => 'Post'])

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
