@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">
                    <a href="/{{'@' . $post->user->username}}">{{$post->user->username}}</a>
                </div>

                <div class="card-body">
                    @if ($post->image)
                            <img src="{{asset('images/post/' . $post->image)}}" alt="Post image" width="100%" height="auto">
                        @endif
                        <p>{{$post->caption}}</p>
                        <button class="btn btn-primary" onclick="like({{$post->id}}, this)">
                            {{ ($post->is_liked() ? 'Unlike' : 'Like') }}
                        </button>
                        <p>
                            <small>{{$post->created_at->diffForHumans()}}</small>
                        </p>
                        {{-- <a class="btn btn-primary" href="/post/{{$post->id}}">Comment</a> --}}
                        <hr>
                        @foreach ($post->comments as $comment)
                            <p>
                                <a href="{{'@' . $comment->user->username}}">{{$comment->user->username}}</a> 
                                {{$comment->body}} - 
                                @if (Auth::user()->id == $comment->user->id)
                                    <a href="/comment/{{$comment->id}}/edit">Edit</a> - 
                                    <a href="/comment/{{$comment->id}}/delete">Delete</a>
                                @endif
                                
                            </p>
                        @endforeach
                        <hr>
                    <form method="POST" action="/comment/{{$post->id}}">
                        @csrf
                        <div class="form-group row">
                            <label for="body" class="col-md-4 col-form-label text-md-right">Your Comment</label>
                            <div class="col-md-6">
                                <textarea id="body" class="textarea" name="body" placeholder="Write comment">{{ old('body', '')}}</textarea>
                            @if ($errors->has('body'))
                                <span class="invalid-feedback" role="alert">
                                    {{ $errors->first('body') }}
                                </span>
                            @endif
                            </div>
                        </div>
                        @include('component.form.submitbtn', ['text' => 'Comment'])
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function like(id, el) {
        fetch('/like/' + id)
            .then(response => response.json())
            .then(data => {
                el.innerText = (data.status == 'LIKE') ? 'Unlike' : 'Like'
            });
    }
</script>
@endsection
