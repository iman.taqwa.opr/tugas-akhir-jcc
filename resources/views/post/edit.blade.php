@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> Edit Post</div>

                <div class="card-body">
                    <form method="POST" action="/post/{{$post->id}}">
                        @csrf
                        @method('PUT')
                        
                        @if ($post->image)
                            <div class="form-group row">
                                <img src="{{asset('images/post/' . $post->image)}}" width="200" height="200">
                            </div>
                        @endif

                        <div class="form-group row">
                            <label for="caption" class="col-md-4 col-form-label text-md-right">Caption kamu</label>
                        
                            <div class="col-md-6">
                <textarea id="caption" class="textarea" name="caption">{{ old('caption', $post->caption)}}</textarea>
                                @if ($errors->has('caption'))
                                    <span class="invalid-feedback" role="alert">
                                        {{ $errors->first('caption') }}
                                    </span>
                                @endif
                            </div>
                        </div>

                        @include('component.form.submitbtn', ['text' => 'Update Post'])

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
