@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> Edit Komentar</div>

                <div class="card-body">
                    <form method="POST" action="/comment/{{$comment->id}}">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label for="body" class="col-md-4 col-form-label text-md-right">Your Comment</label>
                        
                            <div class="col-md-6">
                <textarea id="body" class="textarea" name="body">{{ old('body', $comment->body)}}</textarea>
                                @if ($errors->has('body'))
                                    <span class="invalid-feedback" role="alert">
                                        {{ $errors->first('body') }}
                                    </span>
                                @endif
                            </div>
                        </div>

                        @include('component.form.submitbtn', ['text' => 'Update Comment'])

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
