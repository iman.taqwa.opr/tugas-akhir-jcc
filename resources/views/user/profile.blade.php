@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">{{'@' . $user->username}}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @include('component.avatar')
                    <h5 class="mb-0">{{ $user->fullname }}</h5>
                    <h5 class="mb-0">"{{ $user->bio }}"</h5>
                    @if (Auth::user()->id == $user->id)
                        <a class="btn btn-primary" href="/user/edit">Edit Profile</a>
                    @else
                        <button class="btn btn-primary" onclick="follow({{$user->id}}, this)">
                            {{ (Auth::user()->following->contains($user->id) ? 'Unfollow' : 'Follow') }}
                        </button>
                    @endif

                    <script>
                        function follow(id, el) {
                            fetch('/follow/' + id)
                                .then(response => response.json())
                                .then(data => {
                                    el.innerText = (data.status == 'FOLLOW') ? 'Unfollow' : 'Follow'
                                });
                        }
                    </script>
                </div>
            </div>
            <div class="card">
                <div class="card-header">Feed</div>
                @foreach ($user->posts as $post)
                <div class="card-body">
                        @if ($post->image)
                        <img src="{{asset('images/post/' . $post->image)}}" alt="Post image" width="100%" height="auto">
                        @endif
                        <br>
                        <p>{{$post->caption}}</p>
                        <br>
                        @if (Auth::user()->id == $user->id)
                            <a href="/post/{{$post->id}}/edit">Edit</a>
                        @endif
                        <br>
                        <small>{{$post->created_at->diffForHumans()}}</small>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
