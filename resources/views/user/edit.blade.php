@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Update Profile {{ $user->username }}</div>

                <div class="card-body">
                    <form method="POST" action="/user/edit" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="form-group row">
                            <label for="username" class="col-md-4 col-form-label text-md-right">User Name</label>
                        
                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" 
                                name="username" 
                                value="{{ old('username', $user->username) }}" 
                                required autocomplete="username" autofocus>
                        
                                @if ($errors->has('username'))
                                    <span class="invalid-feedback" role="alert">
                                        {{ $errors->first('username') }}
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fullname" class="col-md-4 col-form-label text-md-right">Full Name</label>
                        
                            <div class="col-md-6">
                                <input id="fullname" type="text" class="form-control @error('fullname') is-invalid @enderror" 
                                name="fullname" 
                                value="{{ old('fullname', $user->fullname) }}" 
                                required autocomplete="fullname" autofocus>
                        
                                @if ($errors->has('fullname'))
                                    <span class="invalid-feedback" role="alert">
                                        {{ $errors->first('fullname') }}
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="bio" class="col-md-4 col-form-label text-md-right">Bio</label>
                        
                            <div class="col-md-6">
                                <input id="bio" type="text" class="form-control @error('bio') is-invalid @enderror" 
                                name="bio" 
                                value="{{ old('bio', $user->bio) }}" 
                                required autocomplete="bio" autofocus>
                        
                                @if ($errors->has('bio'))
                                    <span class="invalid-feedback" role="alert">
                                        {{ $errors->first('bio') }}
                                    </span>
                                @endif
                            </div>
                        </div>
                        @include('component.form.inputfile', ['name' => 'avatar'], ['label' => 'Avatar'])

                        @include('component.form.submitbtn', ['text' => 'Update'])

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
