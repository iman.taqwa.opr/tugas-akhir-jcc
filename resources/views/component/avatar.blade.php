@php
    if ($user->avatar) {
        $avatar_url = asset('images/avatar/' . $user->avatar);
    }
    elseif ($user->fullname) {
        $avatar_url = "https://ui-avatars.com/api/?size=128&name=" . str_replace(' ', '+', $user->fullname);
    }
    else {
        $avatar_url = "https://ui-avatars.com/api/?size=128&name=" . $user->username;
    }
@endphp

<img src="{{$avatar_url}}" alt="Foto profil {{$user->username}}" width="128" height="128" class="rounded-circle">