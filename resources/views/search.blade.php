@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Result for "{{$querySearch}}"</div>
                @forelse ($users as $user)
                <div class="card-body">
                    @include('component.avatar')
                    <br>
                    <a href="/{{'@' . $user->username}}">{{$user->username}}</a>
                    <br>
                    <button class="btn btn-primary" onclick="follow({{$user->id}}, this)">
                        {{ (Auth::user()->following->contains($user->id) ? 'Unfollow' : 'Follow') }}
                    </button>
                </div>
                @empty
                <div class="card-body">
                    <h3>No feed</h3>
                </div>
                @endforelse
            </div>
        </div>
    </div>
</div>
<script>
    function follow(id, el) {
        fetch('/follow/' + id)
            .then(response => response.json())
            .then(data => {
                el.innerText = (data.status == 'FOLLOW') ? 'Unfollow' : 'Follow'
            });
    }
</script>
@endsection