@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">Feed</div>
                @forelse ($posts as $post)
                <div class="card-body">
                    <a href="/{{'@' . $post->user->username}}">{{$post->user->username}}</a>
                    <br>
                    <a href="/post/{{$post->id}}">
                        @if ($post->image)
                        <img src="{{asset('images/post/' . $post->image)}}" alt="Post image" width="100%" height="auto">
                        @endif
                    </a>
                    <p>{{$post->caption}}</p>
                    <button class="btn btn-primary" onclick="like({{$post->id}}, this)">
                        {{ ($post->is_liked() ? 'Unlike' : 'Like') }}
                    </button>
                    <a class="btn btn-primary" href="/post/{{$post->id}}">Comment</a>
                    <p>
                        <small>{{$post->created_at->diffForHumans()}}</small>
                    </p>
                </div>
                @empty
                <div class="card-body">
                    <h3>No feed</h3>
                </div>
                @endforelse
            </div>
        </div>
    </div>
</div>
<script>
    function like(id, el) {
        fetch('/like/' + id)
            .then(response => response.json())
            .then(data => {
                el.innerText = (data.status == 'LIKE') ? 'Unlike' : 'Like'
            });
    }
</script>
@endsection
