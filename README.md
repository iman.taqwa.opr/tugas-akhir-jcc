## Final Project

## Kelompok 19

- Oktavian Pratama Raharja (oktama@outlook.co.id)
- Nurul Ramadhaniah (nurulramadhaniah97@gmail.com)

## Tema Project

Media sosial mini instagram bernama Shahin

## ERD
![ERD-shashin.png](ERD-shashin.png?raw=true)

## Link Video
Link Demo Aplikasi : [demo aplikasi](https://drive.google.com/file/d/1Q6k5w82Gi1u9iJyCKq52z_1fuMAPLVMq/view?usp=sharing) [demo ERD](https://drive.google.com/file/d/12duQE8ABrLzJAqmedvb8hZal20GHKc6g/view?usp=sharing)

Link Deploy: http://sleepy-sea-88218.herokuapp.com/